<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area fullscreen">

		<div id="main" class="site-main" role="main">

			<div class="video-still" style="background-image:url('<?php echo get_field('video_fallback')['url']; ?>')"></div>
			<video playsinline autoplay muted loop id="bgvid">
			    <source src="<?php the_field('video'); ?>" type="video/mp4">
			</video>
			<div class="home-video-filter"></div>

			<div class="home-overlay">
				<div class="svg-container">
					<svg id="ds" width="100%" height="100%">
			            <mask id="cutouttext">
			                <rect width="100%" height="100%" x="0" y="0"  /><?php // moz required ?>
			                <text stroke-miterlimit="10" x="20" y="500">DS</text>
			            </mask>
		            	<rect width="100%" height="100%" x="0" y="0" mask="url(#cutouttext)" />
		        	</svg>
				</div>
				<div class="text-container">
					<span class="white overlay-tag fade-home" data-text="home"><?php echo get_field('overlay_tag'); ?></span>
					<div class="headline-text-container">
						<?php $header_text = get_field('overlay_headline');
							if ($header_text) { 
								foreach ($header_text as $text) {
						?>
								<h1 class="white" data-text="home"><?php echo $text['headline_text']; ?></h1>							
								<?php } ?>
						<?php } ?>
					</div>
					<p class="white" data-text="home"><?php echo get_field('overlay_body'); ?></p>
					<a class="cta-button button-home fade-home" href="<?php echo get_field('overlay_button_url'); ?>"><?php echo get_field('overlay_button_text'); ?></a>
				</div>
			</div>
			<div class="quote-block">
				<span class="white" data-text="home"><?php echo get_field('overlay_quote'); ?></span>
			</div>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>