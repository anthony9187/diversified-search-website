<?php 
	$args = array(
	    'post_type'=> 'offices',
		'posts_per_page' => -1,
	    );              

	$offices = get_posts($args);
	$default_pic = get_template_directory_uri() . '/img/ds-logo.png';
?>

<div class="offices-container">
	<div class="offices-inner-wrap">
		<?php foreach ($offices as $office) { ?>

			<?php 
				$name = get_the_title($office);
				$_search = array("’", ' ', '.');
				$_replace = array('', '-', '');
				$post_slug = get_post_field( 'post_name', get_post($office) );
				$address = get_field('address', $office);
				$image = get_field('image', $office);
				$desc = get_field('description', $office);
				$head = get_field('office_head', $office);
				$directors = get_field('managing_directors', $office);

				if ( $address ) {
					$html = $address;
					$doc = new DOMDocument();
					$doc->loadHTML($html);
					$span = $doc->getElementsByTagName('span')->item(3);
					$city = $doc->saveHTML($span);
				}

				if ($image == false) { 
					$image = get_template_directory_uri() . '/img/ds-logo.png';
				} else { 
					$image = $image['url'];
				};

			?>

			<div class="office-wrap">
				<div class="office">
					<a class="office-thumb" data-id="<?php echo $post_slug; ?>" id="<?php echo $post_slug; ?>" href="#">
						<img src="<?php echo $image; ?>" alt="">
						<div class="office-hover">
							<div><p class="name"><?php echo strtoupper($name); ?></p></div>
						</div>
					</a>
					
					<?php // card ?>
					<div class="office-card" data-id="<?php echo $post_slug; ?>">
						<div class="card-content">
							
							<a href="#" class="button-close button-office-close"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
							<div class="flex-container office-main">
								<div class="office-intro">
									<div class="office-name">
										<h2><?php echo $name; ?></h2>
										<p><?php echo $address; ?></p>
									</div>
									<div class="break"></div>
								</div>

								<div class="office-desc">
									<?php echo $desc; ?>
								</div>
							</div>
							<div class="flex-container office-sidebar">
								<?php if (!empty($head)) { ?>
									<h3>CONTACT</h3>
									<p>Office Managing Director</p>
									<p class="bold"><?php echo $head->post_title; ?></p>
									<?php if (!empty($head->phone)){ echo '<p>Phone: <a class="bold">' . $head->phone . '</a></p>'; } ?>
									<?php if (!empty($head->email)){ echo '<p>E-Mail: <a class="red" href="mailto:' . $head->email . '">' . $head->email . '</a></p>'; } ?>
									<?php if (!empty($head->linkedin)) { echo '<a href="' . $head->linkedin . '" target="_blank"><svg class="linkedin-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-linkedin"></use></svg></a>'; }; ?>
								<?php } ?>

							</div>
							<?php if (!empty($directors)) { ?>
								<div class="flex-container director-container">
									<h3><?php echo strtoupper(get_field('managing_directors_header', $office)); ?></h3>
									<?php foreach ($directors as $director) { ?>
											<?php 	$image = get_field('picture', $director);
													$title = get_field('title', $director);

													if ($image == false) { 
														$image = get_template_directory_uri() . '/img/ds-logo.png';
													} else { 
														$image = $image['sizes']['thumbnail'];
											}; ?>
											<div class="director">
												<a href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ) . '#' . $director->post_name ; ?>"><img class="img-round grayscale-hov" src="<?php echo $image; ?>" alt=""></a>
												<p class="bold"><?php echo strtoupper($director->post_title); ?></p>
												<p><?php echo $title ?></p>
											</div>
									<?php } ?>
								</div>
							<?php } ?>
							
						</div>
					</div>
				</div>
			</div>
			
		<?php } ?>
	</div>

</div>