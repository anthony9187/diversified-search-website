<?php
	$default_pic = get_template_directory_uri() . '/img/ds-logo.png';
?>

	<div class="people-container">
		<div class="people-inner-wrap">
			<?php if ( have_rows('roles') ) : ?>

				<?php while ( have_rows('roles') ) : the_row(); ?>

					<?php $people = get_sub_field('role_staff'); ?>

					<?php if ( $people ) : ?>

						<h2><?php the_sub_field('role_name'); ?></h2>
						
						<?php foreach ($people as $person) { ?>
							
							<?php //setup_postdata( $person ); ?>


								<?php
									$name = strtoupper(get_the_title($person));
									$post_slug = get_post_field( 'post_name', get_post($person) );
									$title = get_field('title', $person);
									$bio = get_field('bio', $person);
									$phone = get_field('phone', $person);
									$email = get_field('email', $person);
									$linkedin = get_field('linkedin', $person);
									$vcf = get_field('vcf', $person);
									$educations = get_field('education', $person);
									$practices = get_field('practice_areas', $person);
									$location = get_field('office', $person);

									$pic = get_field('picture', $person);									
									if ($pic == false) { 
										$pic = get_template_directory_uri() . '/img/ds-logo.png';
									} else { 
										$pic = $pic['sizes']['thumbnail'];
									};

								?>
								<div class="person">
									<div class="person-thumb" data-id="<?php echo $post_slug; ?>" id="<?php echo $post_slug; ?>" href="#">
										<img src="<?php echo $pic; ?>" alt="">
										<div class="person-hover">
											<div><p class="name white"><?php echo $name; ?></p></div>
											<div><p class="title white"><?php echo $title; ?></p></div>
										</div>
									</div>
									
									<?php // card ?>
									<div class="person-card" data-id="<?php echo $post_slug; ?>" id="<?php echo $post_slug; ?>">
										<a href="#" class="button-close button-person-close"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
										<div class="person-main">
											<div class="person-intro">
												<img class="img-round" src="<?php echo $pic ?>" alt="">
												<div class="person-name">
													<h4><?php echo $name; ?></h4>
													<p><?php echo $title; ?></p>
												</div>
												<div class="break"></div>
											</div>

											<div class="person-bio">
												<?php echo $bio; ?>
											</div>
										</div>
										
										<div class="person-sidebar">
											<?php if ( !empty($phone) || !empty($email) || !empty($location) || !empty($linkedin) ) { ?>
												<h3>CONTACT</h3>
												<?php if (!empty($phone)){
													echo '<p>Phone: <strong>' . $phone . '</strong></p>'; 
												} ?>

												<?php if (!empty($email)){
													echo '<p>E-Mail: <a href="mailto:' . $email . '">' . $email . '</a></p>'; 
												} ?>

												<?php if (!empty($location)){
													$offices = array();
													foreach ($location as $l) {

														$l_slug = get_post_field( 'post_name', get_post($l) );
														$l_address = get_field( 'address', $l );

														if ( !empty($l_address) ) {
															$html = $l_address;
															$doc = new DOMDocument();
															$doc->loadHTML($html);

															$span_city = $doc->getElementsByTagName('span')->item(3);
															$city = strip_tags($doc->saveHTML($span_city));

															if ($city === 'Washington, D.C.') {
																$l_title = $city;
															} else {
																$span_state = $doc->getElementsByTagName('span')->item(4);
																$state = strip_tags($doc->saveHTML($span_state));
																$l_title = $city . ', ' . $state;
															}
														}

														$offices[] = '<a class="bold" href=' . get_permalink( get_page_by_path( 'about-us/us-offices' ) ) . '#' . $l_slug . '>' . $l_title . '</a>';
													}
													$office_list = implode(' / ', $offices);
													 echo '<p>Location: ' . $office_list . '</p>';
												} ?>

												<?php if (!empty($linkedin)){
													echo '<a href="' . $linkedin . '" target="_blank"><svg class="linkedin-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-linkedin"></use></svg></a>'; 
												} ?>

												<?php if ($vcf !== false){
													echo '<a href="' . $vcf['url'] . '"><svg class="vcf-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-vcard"></use></svg></a>'; 
												} ?>

											<?php }; ?>
												
											<?php if( !empty($educations) ) { ?>
												<h3>EDUCATION</h3>
												<?php foreach ($educations as $education) { ?>
													<p class="bold"><?php echo $education['degree']; ?></p>
													<p><?php echo $education['university']; ?></p>
												<?php } ?>
											 <?php } ?>

											 <?php if( !empty($practices) ) { ?>
											 	<h3>PRACTICE AREAS</h3>
												<?php foreach ($practices as $practice) { ?>
													<?php $practice_link = get_site_url() . '/our-expertise/' . get_the_terms($practice, 'type')[0]->slug . 's/#' . $practice->post_name; ?>
													<a class="practice-area blue" href="<?php echo $practice_link; ?>"><?php echo strtoupper($practice->post_title); ?></a>
												<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>

						<?php } ?>

						<?php //wp_reset_postdata(); ?>
					    
					<?php endif; ?>

				<?php endwhile; ?>

			<?php endif; ?>

		</div>

	</div>

<?php //} ?>