<?php
	$name = get_the_title();
	$_search = array("’", ' ', '.');
	$_replace = array('', '-', '');
	$id = str_replace($_search, $_replace, (strtolower(html_entity_decode($name))));
	$title = get_field('title');
	$pic = get_field('picture');
	$bio = get_field('bio');
	$phone = get_field('phone');
	$email = get_field('email');
	$linkedin = get_field('linkedin');
	$location = get_field('location');
	$location_slug = str_replace(' ', '-', (strtolower((strtok($location, ',')))));
	$vcf = get_field('vcf');
	$office = get_field('office');
	$educations = get_field('education');
	$practices = get_field('practice_areas');					
	

	if ($pic == false) { 
		$pic = get_template_directory_uri() . '/img/ds-logo.png';
	} else { 
		$pic = $pic['sizes']['thumbnail'];
	};

?>
<div class="person-wrap">
	<div class="person">
		<div class="person-thumb" data-id="<?php echo $id; ?>" href="#">
			<img src="<?php echo $pic; ?>" alt="">
			<div class="person-hover<?php if($page == 'our-leadership') { echo ' leadership'; } ?>">
				<div><p class="name white"><?php echo strtoupper($name); ?></p></div>
				<div><p class="title white"><?php echo $title; ?></p></div>
				<?php if($page == 'our-leadership') { ?>
					<div class="contact-buttons">
						<a href="<?php echo $linkedin ?>"><svg class="phone-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-phone"></use></svg></a>
						<a href="<?php echo $linkedin ?>"><svg class="mail-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-mail"></use></svg></a>
						<a href="<?php echo $linkedin ?>"><svg class="linkedin-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-linkedin"></use></svg></a>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<!-- card -->
		<div class="person-card" data-id="<?php echo $id; ?>" id="<?php echo $id ?>">
			<a href="#" class="button-close button-person-close"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
			<div class="person-main">
				<div class="person-intro">
					<img class="person-img-round" src="<?php echo $pic ?>" alt="">
					<div class="person-name">
						<h4><?php echo $name; ?></h4>
						<p><?php echo $title; ?></p>
					</div>
					<div class="break"></div>
				</div>

				<div class="person-bio">
					<?php echo $bio; ?>
				</div>
			</div>
			
			<div class="person-sidebar">
				<?php if ( !empty($phone) || !empty($email) || !empty($location) || !empty($linkedin) ) { ?>
					<h3>CONTACT</h3>
					<?php if (!empty($phone)){ echo '<p>Phone: <strong>' . $phone . '</strong></p>'; } ?>
					<?php if (!empty($email)){ echo '<p>E-Mail: <a href="mailto:' . $email . '">' . $email . '</a></p>'; } ?>
					<?php if (!empty($location)){ echo '<p>Location: <a class="bold" href=' . get_site_url() . '/about-us/us-offices/'. '#' . $location_slug . '>' . $location . '</a></p>'; } ?>
					<?php if (!empty($linkedin)) { echo '<a href="' . $linkedin . '" target="_blank"><svg class="linkedin-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-linkedin"></use></svg></a>'; }; ?>
					<?php if ($vcf !== false) { echo '<a href="' . $vcf['url'] . '"><svg class="vcf-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-vcard"></use></svg></a>'; }; ?>
					<?php }; ?>
					
				<?php if( !empty($educations) ) { ?>
					<h3>EDUCATION</h3>
					<?php foreach ($educations as $education) { ?>
						<p class="bold"><?php echo $education[degree]; ?></p>
						<p><?php echo $education[university]; ?></p>
					<?php } ?>
				 <?php } ?>

				 <?php if( !empty($practices) ) { ?>
				 	<h3>PRACTICE AREAS</h3>
					<?php foreach ($practices as $practice) { ?>
						<?php $practice_link = get_site_url() . '/our-expertise/' . get_the_terms($practice, 'type')[0]->slug . 's/#' . $practice->post_name; ?>
						<a class="practice-area blue" href="<?php echo $practice_link; ?>"><?php echo strtoupper($practice->post_title); ?></a>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>