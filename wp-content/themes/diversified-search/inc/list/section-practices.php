<?php

	if ( get_field('functional_practices_order') ) { 
		$items = get_field('functional_practices_order');

	} elseif ( get_field('industry_practices_order') ) { 
		$items = get_field('industry_practices_order');
	};

	$default_pic = get_template_directory_uri() . '/img/ds-logo.png';
?>

<div class="items-container">
	<div class="items-inner-wrap">
		<?php foreach ($items as $item) { ?>

			<?php 
				$name = get_the_title($item);
				$_search = array("’", ' ', '.', ',');
				$_replace = array('', '-', '', '');
				$post_slug = get_post_field( 'post_name', get_post($item) );
				$image = get_field('image', $item);
				$desc = get_field('description', $item);
				$heads	= get_field('practice_leaders', $item);
				$directors = get_field('managing_directors', $item);
				$resume_url = get_field('resume_submission_url', $item);

				if( $post_type == 'functional-practice') {
					$functions = get_field('functions_list', $item);
				}
				$sectors = get_field('sectors_list', $item);

				if ($image == false) { 
					$image = get_template_directory_uri() . '/img/ds-logo.png';
				} else { 
					$image = $image['url'];
				};

			?>

			<div class="item-wrap">
				<div class="item">
					<a class="item-thumb" data-id="<?php echo $post_slug; ?>" href="#">
						<img src="<?php echo $image; ?>" alt="">
						<div class="item-hover">
							<div><p class="name"><?php echo strtoupper($name); ?></p></div>
						</div>
					</a>
					
					<?php // card ?>
					<div class="item-card" data-id="<?php echo $post_slug; ?>" id="<?php echo $post_slug ?>">
						<div class="card-content">
							<a href="#" class="button-close button-item-close"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>

							<div class="flex-top">
								<div class="item-title">
									<h2><?php echo $name; ?></h2>
									
									<div class="break"></div>	
									
									<?php echo $desc; ?>

									<div class="break"></div>
								</div>

								<?php if (!empty($sectors)) { ?> 
									<div class="item-sector">
										<h3>SECTORS</h3>
										<?php foreach ($sectors as $sector) { echo '<p>' . $sector['sector_name'] . '</p>'; } ?>
									</div>
								<?php } ?>

								
								<?php if (!empty($functions)) { ?>
									<div class="item-function">
										<h3>FUNCTIONS</h3>
										<?php foreach ($functions as $function) { echo '<p>' . $function['function_name'] . '</p>'; } ?>
									</div>
								<?php } ?>
							</div>

							<?php if (!empty($heads)) { ?>
								<?php if (count($heads) === 1 ) { ?>
									<h3>PRACTICE LEADER</h3>
								<?php } else { ?>
									<h3>PRACTICE LEADERS</h3>
								<?php } ?>
								<div class="item-leader-container">
										<?php foreach ($heads as $head) { ?>
											<div class="item-leader">
												<?php $image = get_field('picture', $head); ?>
												<?php 
														 if ( isset($image) ) { 
															$image = $image['sizes']['thumbnail'];
														} else { 
															
															$image = get_template_directory_uri() . '/img/ds-logo.png';
														};
												?>
												<a href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ) . '#' . $head->post_name ; ?>">
													<img class="img-round" src="<?php echo $image; ?>" alt="">
													<div class="leader-name">
														<h4><?php echo $head->post_title; ?></h4>
														<p><?php echo $head->title; ?></p>
													</div>
												</a>
												<div class="leader-contact">
													<?php if (!empty($phone)){ echo '<p>Phone: <a class="bold">' . $head->phone . '</a></p>'; } ?>
													<?php if (!empty($email)){ echo '<p>E-Mail: <a class="red" href="mailto:' . $head->email . '">' . $head->email . '</a></p>'; } ?>
													<div class="linkedin-container"><?php if (!empty($linkedin)) { echo '<a href="' . $head->linkedin . '" target="_blank"><svg class="linkedin-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-linkedin"></use></svg></a>'; }; ?></div>
												</div>
											</div>
										<?php } ?>
									</div>
							<?php } ?>
							
							<?php if ( isset($resume_url) ) { ?>
								<div class="item-resume">
									<div class="submit-resume">
										<a class="submit white" href="<?php echo $resume_url; ?>" target="_blank">
											<svg class="cv-svg">
												<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-cv"></use>
											</svg>
											<img class="resume-arrow" src="<?php echo esc_url(get_template_directory_uri().'/img/resume-arrow.svg'); ?>" alt="share-your-resume">
										</a>
									</div>
								</div>
							<?php } ?>
							
							<div class="item-director">
								<?php if (!empty($directors)) { ?>
									<h3><?php echo strtoupper( get_field('staff_header', $item) ); ?></h3>
									<div class="director-container">
										<?php foreach ($directors as $director) { ?>
												<?php 	$image = get_field('picture', $director);
														$title = get_field('title', $director);

														if ( !empty($image) ) { 
															$image = $image['sizes']['thumbnail'];
														} else { 
															
															$image = get_template_directory_uri() . '/img/ds-logo.png';
														}; 
												?>
												<div class="director">
													<a href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ) . '#' . $director->post_name ; ?>">
														<img class="img-round grayscale-hov" src="<?php echo $image; ?>" alt="">
														<p><?php echo strtoupper($director->post_title); ?></p>
														<p><?php echo $title ?></p>
													</a>
												</div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>

						</div>
					</div>
					<?php // end card ?>
				</div>
			</div>
			
		<?php } ?>
	</div>

</div>