<?php
	$leaders = get_field('leaders');
	$default_pic = get_template_directory_uri() . '/img/ds-logo.png';
?>

<div class="people-container">
	<div class="people-inner-wrap">
		
		<?php if ( $leaders ) : ?>
			
			<?php foreach ($leaders as $leader) { ?>
				
				<?php setup_postdata( $post ); ?>

				<?php
					$name = get_the_title($leader);
					$post_slug = get_post_field( 'post_name', get_post($leader) );
					$title = get_field('title', $leader);
					$pic = get_field('picture', $leader);
					$bio = get_field('bio', $leader);
					$phone = get_field('phone', $leader);
					$email = get_field('email', $leader);
					$linkedin = get_field('linkedin', $leader);
					$educations = get_field('education', $leader);
					$practices = get_field('practice_areas', $leader);
					$vcf = get_field('vcf', $leader);
					$location = get_field('office', $leader);

					if ($pic == false) { 
						$pic = get_template_directory_uri() . '/img/ds-logo.png';
					} else { 
						$pic = $pic['sizes']['thumbnail'];
					};

				?>
					<div class="person">
						<div class="person-thumb" data-id="<?php echo $post_slug; ?>" href="#">
							<img src="<?php echo $pic; ?>" alt="">
							<div class="person-hover leadership">
								<div><p class="name white"><?php echo strtoupper($name); ?></p></div>
								<div><p class="title white"><?php echo $title; ?></p></div>
								<div class="contact-buttons" style="display: none">
									<a href="#"><svg class="phone-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-phone"></use></svg></a>
									<a href="<?php echo 'mailto:' . $email ?>"><svg class="mail-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-mail"></use></svg></a>
									<a href="<?php echo $linkedin ?>" target="_blank"><svg class="linkedin-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-linkedin"></use></svg></a>
								</div>
							</div>
						</div>
						
						<!-- card -->
						<div class="person-card" data-id="<?php echo $post_slug; ?>">
							<a href="#" class="button-close button-person-close"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
							<div class="person-main">
								<div class="person-intro">
									<img class="img-round" src="<?php echo $pic ?>" alt="">
									<div class="person-name">
										<h4><?php echo $name; ?></h4>
										<p><?php echo $title; ?></p>
									</div>
									<div class="break"></div>
								</div>
		
								<div class="person-bio">
									<p><?php echo $bio; ?></p>
								</div>
							</div>
							
							<div class="person-sidebar">
								<?php if ( !empty($phone) || !empty($email) || !empty($location) ) { ?>
									<h3>CONTACT</h3>
									<?php if (!empty($phone)){
										echo '<p>Phone: <strong>' . $phone . '</strong></p>';
									} ?>
									<?php if (!empty($email)){
										echo '<p>E-Mail: <a class="red" href="mailto:' . $email . '">' . $email . '</a></p>'; 
									} ?>
									<?php if (!empty($location)){
									$offices = array();
									foreach ($location as $l) {

										$l_slug = get_post_field( 'post_name', get_post($l) );
										$l_address = get_field( 'address', $l );

										if ( !empty($l_address) ) {
											$html = $l_address;
											$doc = new DOMDocument();
											$doc->loadHTML($html);

											$span_city = $doc->getElementsByTagName('span')->item(3);
											$city = strip_tags($doc->saveHTML($span_city));

											if ($city === 'Washington, D.C.') {
												$l_title = $city;
											} else {
												$span_state = $doc->getElementsByTagName('span')->item(4);
												$state = strip_tags($doc->saveHTML($span_state));
												$l_title = $city . ', ' . $state;
											}
										}

										$offices[] = '<a class="bold" href=' . get_permalink( get_page_by_path( 'about-us/us-offices' ) ) . '#' . $l_slug . '>' . $l_title . '</a>';
									}
									$office_list = implode(' / ', $offices);
									 echo '<p>Location: ' . $office_list . '</p>';
								} ?>
									<?php if (!empty($linkedin))
										{ echo '<a href="' . $linkedin . '" target="_blank"><svg class="linkedin-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-linkedin"></use></svg></a>';
									} ?>
									<?php if ($vcf !== false)
										{ echo '<a href="' . $vcf['url'] . '"><svg class="vcf-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-vcard"></use></svg></a>';
									} ?>
								<?php } ?>
									
								<?php if( !empty($educations) ) { ?>
									<h3>EDUCATION</h3>
									<?php foreach ($educations as $education) { ?>
										<p class="bold"><?php echo $education['degree']; ?></p>
										<p><?php echo $education['university']; ?></p>
									<?php } ?>
								 <?php } ?>

								 <?php if( !empty($practices) ) { ?>
								 	<h3>PRACTICE AREAS</h3>
									<?php foreach ($practices as $practice) { ?>
										<?php $practice_link = get_site_url() . '/our-expertise/' . get_the_terms($practice, 'type')[0]->slug . 's/#' . $practice->post_name; ?>
										<a class="practice-area blue" href="<?php echo $practice_link; ?>"><?php echo strtoupper($practice->post_title); ?></a>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>

			<?php }; ?>

		<?php else: ?>
		    <h2>Sorry...</h2>
		    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	    	<?php wp_reset_postdata(); ?>

	<?php endif; ?>

	</div>

</div>
<div class="section-quote leadership-quote default">
	<p><svg class="svg-quote open">
		<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-open"></use>
	</svg><?php the_field('default_quote'); ?><svg class="svg-quote close">
		<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-close"></use>
	</svg></p>
	<p class="attribution"><?php the_field('default_quote_attribution'); ?></p>
</div>
<?php if ( $leaders ) : ?>
			
		<?php foreach ($leaders as $leader) { ?>

			<div class="section-quote leadership-quote leader-quote" data-id="<?php echo get_post_field( 'post_name', get_post($leader) ); ?>">
				<p><svg class="svg-quote open">
					<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-open"></use>
				</svg><?php the_field('quote', $leader); ?><svg class="svg-quote close">
					<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-close"></use>
				</svg></p>
				<p class="attribution"><?php the_field('quote_attribution', $leader); ?></p>
			</div>

		<?php } ?>

<?php endif; ?>








