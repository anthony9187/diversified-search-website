<div class="nav-slider">
	<a href="#" class="button-close home"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
	<div class="nav-slider-container">
		
			<?php wp_nav_menu( $args = array() ); ?>

		<div class="contact">
			<div class="address">
				<p class="bold">HEADQUARTERS</p>
				<p><?php echo get_theme_mod('nav_address_street'); ?></p>
				<p><?php echo get_theme_mod('nav_address_cityStateZip'); ?></p>
				<p><?php echo get_theme_mod('nav_address_country'); ?></p>
				<a class="white" href="mailto:Steve.Morreale@divsearch.com">Contact Us</a>
			</div>
			<div class="inquire">
				<p>For general inquiries, please call us at</p>
				<p>1.800.423.3932</p>
			</div>
			<div class="candidate">
				<p class="bold">BE A CANDIDATE</p>
				<p>We'll help you find your next leadership position</p>
				<a class="white" href="https://searchlight.cluen.com/E5/Login.aspx?URLKey=nqqzejyu" target="_blank">Submit Resume</a>
			</div>
			<div class="social">
				<?php 
					$nav_social = get_theme_mods( 'nav_social_section' );
					$nav_array = array();
				
					foreach ($nav_social as $key => $value ) {
						if (substr( $key, 0, 11 ) === "nav_social_") {
							$network = substr($key, 11);
							echo '<a href="' . $value . '" target="_blank"><svg class="' . $network . '-svg"><use xlink:href="' . get_template_directory_uri() . '/img/spritemap.svg#icon-' . $network . '"></use></svg></a>';
						}
					}
				?>

			</div>
		</div>
	</div>
	<div class="menu-trademark">
		<span>Copyright Diversified Search <?php echo date('Y'); ?> | All Rights Reserved | Designed By <a class="white" href="https://2one5.com" target="_blank">[ 2one5 ] Creative</a></span>
	</div>
</div>