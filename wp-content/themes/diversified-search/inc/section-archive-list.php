<div class="container">

	<?php
		$post_type = get_post_type();
		$custom_tax = 'category-' . $post_type;
	
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		$wpb_all_query = new WP_Query(array('post_type'=>get_post_type(),
											'post_status'=>'publish',
											'posts_per_page' => 6,
											'paged' => $paged
											)
										); 

		if ( $wpb_all_query->have_posts() ) : 

	?>

		<div class="post-list-wrap">

			<!-- the loop -->
			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
				<?php 
					$body = substr(get_field('body', false, false), 0, 400) . '...';
					$image = get_field('hero');
					$cat = wp_get_post_terms($post->ID, $custom_tax);

					if ($image == false) { 
						$image = get_template_directory_uri() . '/img/ds-logo.png';
					} else {
						$image = $image['sizes']['medium'];
					}
				?>
				<div>
					<div class="post-preview">
						<div class="post-preview-img">
							<img src="<?php echo $image; ?>" alt="">
						</div>
						<div class="post-preview-text">
							<?php if(!empty($cat)) { ?>
								<a class="red" href="<?php echo get_post_type_archive_link( $post_type ) . $cat[0]->slug; ?>"><?php echo $cat[0]->name; ?></a>
							<?php } ?>
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<p><?php echo $body; ?><a href="<?php the_permalink(); ?>">Read More</a></p>
						</div>
					</div>
					<div class="break"></div>
				</div>
			<?php endwhile; ?>

			<?php // end of loop ?>
			
			<?php if($wpb_all_query->max_num_pages > 1) : ?>

				<?php previous_posts_link('Newer Entries'); ?>
				
				<?php next_posts_link( 'Older Entries' ); ?>

			<?php endif; ?>



		</div>

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
</div>