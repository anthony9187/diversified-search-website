<div class="toolbar">
	
	<?php //where we do business sort ?>
	<?php if ( !empty($page_name) && $page_name == 'where-we-do-business') { ?>
		<?php $regions = get_field('region'); ?>

		<?php if ($regions) { ?>
				<a class="dropdown-btn region" data-cat="region">STATE<span><svg class="play-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-play"></use></svg></span></a>

				<div class="dropdown-content region" data-cat="region">
					
					<a href="#" class="button-close region"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>

					<div class="content-wrap">
						<?php foreach ($regions as $region) { ?>
							<?php $reg_name = $region['region_name']; ?>
							<a href="#" data-region="<?php echo $reg_name ?>" class="dropdown-select white"><?php echo $reg_name ?></a>
						<?php } ?>
					</div>

				</div>
		<?php }  ?>
	<?php }?>

	<?php //blog sort ?>
	<?php if( ( is_archive()) ) { 
		$post_type_cat = 'category-' . get_post_type();
		$post_type_obj = get_post_type_object( get_post_type($post) ); 
		$args = array( 'taxonomy'=> $post_type_cat, 'hide_if_empty'=> false, 'echo'=> 0 ); 
		$categories = get_categories($args);
		if ($categories == true) {
		?>
			<a class="dropdown-btn category" data-cat="cat">CATEGORIES<span><svg class="play-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-play"></use></svg></span></a>
			<div class="dropdown-content category" data-cat="cat">
				<a href="#" class="button-close categories"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>

				<div class="content-wrap">
					<a href="<?php echo get_site_url() . '/' . get_post_type(); ?>" class="dropdown-select white"><?php echo 'All ' . $post_type_obj->label; ?></a>
					<?php foreach ($categories as $cat) { ?>
						<a href="<?php echo get_site_url() . '/' . get_post_type() . '/' . $cat->slug ?>" class="dropdown-select white"><?php echo ucwords($cat->name) ?></a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php //our people sort  ?>
	<?php if (!empty($page_name)) { ?>
		<?php if ($page_name === 'our-people' || $page_name === 'our-people-search-results') { ?>

			<?php //search ?>
			<div class="search-toolbar-people">
				<div class="search-wrap-people">
					<div class="search-people">
						<label class="screen-reader-text srt-only" for="s">Search:</label>
						<form id="searchform" action="<?php echo get_permalink( get_page_by_title( 'Our People Search Results' ) ); ?>" method="get">
							<input type="text" name="search" placeholder="SEARCH PEOPLE" onfocus="this.placeholder = ''" onblur="this.placeholder = 'SEARCH PEOPLE'" />
						</form>
						<svg class="search-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-search"></use></svg>
					</div>
				</div>
			</div>
			
			<?php //office sort ?>
			<a class="dropdown-btn office" data-cat="office">OFFICE<span><svg class="play-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-play"></use></svg></span></a>
			<div class="dropdown-content office" data-cat="office">
				<a href="#" class="button-close office"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
				<div class="content-wrap">
					<a href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ); ?>" class="dropdown-select white"><?php echo 'All People' ?></a>

					<?php $office_ids = get_posts(array(

							'post_type' => 'offices',
							'orderby' => 'title',
							'posts_per_page'=> -1,
							'order' => 'ASC',
							'fields' => 'ids'
							));

						foreach ($office_ids as $id) { ?>
							
							<a href="<?php echo get_permalink( get_page_by_title( 'Our People Search Results' ) ) . '?office=' . $id ?>" class="dropdown-select white"><?php echo ucwords(get_the_title($id)) ?></a>

					<?php } ?>
					
				</div>
			</div>

			<?php //practices sort ?>
			<a class="dropdown-btn practice" data-cat="practice">PRACTICE AREAS<span><svg class="play-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-play"></use></svg></span></a>
			<div class="dropdown-content practice" data-cat="practice">
				<a href="#" class="button-close practice"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>

				<div class="content-wrap">
					<a href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ); ?>" class="dropdown-select white"><?php echo 'All People' ?></a>
					<?php $practice_ids = get_posts(array(

							'post_type' => 'practices',
							'orderby' => 'title',
							'order' => 'ASC',
							'posts_per_page'=> -1,
							'fields' => 'ids'
							));
						foreach ($practice_ids as $id) { ?>
							
							<a href="<?php echo get_permalink( get_page_by_title( 'Our People Search Results' ) ) . '?practice=' . $id ?>" class="dropdown-select white"><?php echo ucwords(get_the_title($id)) ?></a>

					<?php } ?>
				</div>
			</div>
		

		<?php } ?>
	<?php } ?>


</div>