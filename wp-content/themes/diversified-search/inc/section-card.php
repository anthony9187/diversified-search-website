<?php
	$args = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $post->ID,
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
	 );

	$parent = new WP_Query( $args );
?>

<div class="cat-title-container">
	<div class="cat-title-content">

		<div class="svg-container" id="<?php echo( basename(get_permalink()) );?>">

			<svg id="<?php echo( basename(get_permalink()) );?>" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 868.04 217.65"><title><?php the_title(); ?></title><text transform="translate(0, 134)" stroke-miterlimit="10"><?php echo strtoupper(get_the_title()); ?></text></svg>

		</div>
		<p class="white" data-hide="hide"><?php the_field('sub_header') ?></p>
		<?php if ( $parent->have_posts() ) {  ?>
		
				<div class="cat-btn-container" data-hide="hide">
				    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				            
				            <div class="btn-wrap">
				            	<a class="link-button" href="<?php the_permalink(); ?>"><?php echo strtoupper(get_the_title()); ?></a>
			            	</div>
			            	
		    		<?php endwhile; ?>

				</div>

		<?php }; wp_reset_query(); ?>

		<div class="category-quote white">
			<p><?php the_field('category_quote'); ?></p>
			<p><?php the_field('category_quote_attribution'); ?></p>
		</div>
	</div>
</div>