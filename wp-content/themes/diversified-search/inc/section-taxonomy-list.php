<div class="container">

	<?php
		$tax = $post_title->taxonomy;
		$slug = $post_title->slug;
		$post_type_name = $post->post_type;
		$custom_tax = 'category-' . $post_type_name;
		
		$terms = get_terms( array(
			'taxonomy' => $tax,
			'slug' => $slug,
		    'orderby'    => 'count',
		    'hide_empty' => 0
		) );

		foreach( $terms as $term ) {
	 

		    $args = array(
		        'post_type' => $post_type_name,
		        $custom_tax => $term->slug
	    );

	    $query = new WP_Query( $args );
	?>
		<?php if ( $query->have_posts() ) :  ?>
			<div class="post-list-wrap">

				<!-- the loop -->
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<?php 
						$body = substr(get_field('body', false, false), 0, 400) . '...';
						$image = get_field('hero');

						if ($image == false) { 
							$image = get_template_directory_uri() . '/img/ds-logo.png';
						} else {
							$image = $image['sizes']['medium'];
						}
					?>
					<div class="post-preview">
							<div class="post-preview-img">
								<img src="<?php echo $image; ?>" alt="">
							</div>
						<div class="post-preview-text">
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
							<p><?php echo $body; ?><a href="<?php echo get_permalink(); ?>">Read More</a></p>
						</div>
					</div>
					<div class="break"></div>
				<?php endwhile; ?>
				<!-- end of the loop -->
		 		<?php if( get_previous_posts_link() ) {

					previous_posts_link( 'Newer Entries' );

				} ?>

				<?php if( get_next_posts_link() ) {

					next_posts_link( 'Older Entries' );

				} ?>
				<a class="cta-button back white" href="<?php echo get_post_type_archive_link( $post_type_name );?>">
					<svg class=""><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-arrow-left"></use></svg>
					<span class="white">View All <?php echo get_post_type_object($post->post_type)->label; ?></span>
				</a>

			</div>

			<?php wp_reset_postdata(); ?>

		<?php else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
	<?php } ?>
</div>