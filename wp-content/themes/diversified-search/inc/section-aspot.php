<?php
	if ( is_post_type_archive( 'news' ) && get_field('background_image_news', 'option') ) {
    
        $heroImage = get_field('background_image_news', 'option');

        if ( get_field('aspot_subheader_news', 'option') ) {
            $heroSubheader = get_field('aspot_subheader_news', 'option');
        }

    } elseif ( is_post_type_archive( 'insights' ) && get_field('background_image_insights', 'option') ) {
    
        $heroImage = get_field('background_image_insights', 'option');

        if ( get_field('aspot_subheader_insights', 'option') ) {
            $heroSubheader = get_field('aspot_subheader_insights', 'option');
        }

    } elseif ( is_post_type_archive( 'success-stories' ) && get_field('background_image_success_stories', 'option') ) {
    
        $heroImage = get_field('background_image_success_stories', 'option');

        if ( get_field('aspot_subheader_success_stories', 'option') ) {
            $heroSubheader = get_field('aspot_subheader_success_stories', 'option');
        }

    } elseif (is_tax() && get_the_terms( $post->ID, 'category-insights' ) && get_field('background_image_insights', 'option') ) {
    
        $heroImage = get_field('background_image_insights', 'option');

    } elseif (is_tax() && get_the_terms( $post->ID, 'category-success-stories' ) && get_field('background_image_success_stories', 'option') ) {
    
        $heroImage = get_field('background_image_success_stories', 'option');

    } else {

    	$heroImage = get_field('hero_image');

    }

    $backgroundImage = $heroImage['sizes']['large'];

?>

<div class="aspot-container" style="background-image: url('<?php echo $backgroundImage; ?>')">

<div class="aspot-filter"></div>

	<?php if(get_post_type() == 'page') { ?>
		
        <h1 class="page-title"><?php echo strtoupper(get_the_title()); ?></h1>

    <?php } elseif ( is_tax() ) { ?>

        <?php $post_title = get_queried_object(); ?>

        <h1 class="page-title"><?php echo strtoupper($post_title->name); ?></h1>
	
    <?php } else { ?>
	
    	<?php $post_title = get_queried_object(); ?>

        <h1 class="page-title"><?php echo strtoupper($post_title->labels->name); ?></h1>
	
    <?php } ?>
    
    <?php if (!empty($heroSubheader)) { ?>
    
        <p class="page-subtitle"><?php echo $heroSubheader; ?></p>
    
    <?php } ?>

</div>