<?php
/**
 * Template Name: Blog List Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<div id="main" class="site-main bg-gray" role="main">
			
			<?php 
				include 'inc/section-aspot.php';
			
				include 'inc/section-toolbar.php';

				include 'inc/section-archive-list.php'; 
			?>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>