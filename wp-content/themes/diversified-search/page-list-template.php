<?php
/**
 * Template Name: List Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

$page_name = str_replace(' ', '-', ( strtolower( get_the_title() ) ) );
$toolbar = get_field('toolbar');
$subheader = get_field('subheader');

get_header(); ?>
	
	<div id="primary" class="content-area">

		<div id="main" class="site-main" role="main">

			<?php include 'inc/section-aspot.php'; ?>

			<?php if($subheader) { ?>
				<p class="subheader"><?php the_field('subheader'); ?></p>
			<?php } ?>

			<?php if (($page_name == 'functional-practices') || ($page_name == 'industry-practices')) { ?> 
				<?php include 'inc/list/section-practices.php' ?>
			<?php } else { ?> 
				<?php include 'inc/list/section-' . $page_name . '.php' ?>
			<?php } ?> 

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>