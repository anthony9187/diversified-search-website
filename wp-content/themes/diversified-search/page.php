<?php get_header() ?>

<!--page.php-->

<div class="container">
	<?php while ( have_posts() ) { the_post(); ?>
		<?php the_title() ?>
		<?php the_content() ?>
	<?php } ?>
</div>
<?php //get_sidebar();?>
<?php get_footer() ?>