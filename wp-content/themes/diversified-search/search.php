<?php
get_header();
global $wp_query;
?>
<div id="primary" class="content-area">
  <div id="main" class="site-main bg-gray" role="main">
    <div class="body-wrapper search-results">
      <h2 class="search-title"> <?php echo $wp_query->found_posts; ?>
        <?php _e( 'Search Result(s) Found For', 'locale' ); ?>: <?php the_search_query(); ?> </h2>
        <div class="search-form-wrap">
            <?php get_search_form(); ?>
        </div>
        <?php if ( have_posts() ) { ?>

            <ul>

            <?php while ( have_posts() ) { the_post(); ?>

            	<?php switch ($post->post_type) {

            		case 'people':
            			$link = get_permalink( get_page_by_path( 'our-people' ) ) . '#' . $post->post_name;
            			$body = $body = substr(get_field('bio'), 0, 400) . '...';
            			break;

            		case 'offices':
            			$link = get_permalink( get_page_by_path( 'about-us/us-offices' ) ) . '#' . $post->post_name;
            			$body = $body = substr(get_field('description'), 0, 400) . '...';
            			break;

        			case 'practices':
        				$type = get_the_terms( $post, 'type' )[0]->slug;
            			$link = get_permalink( get_page_by_path( 'our-expertise' )) . $type . 's/#' . $post->post_name;
            			$body = $body = substr(get_field('description'), 0, 400) . '...';
            			break;

        			case 'news': 
        			case'insights': 
        			case 'success-stories':
        				$blog = true;
                        $body = get_first_paragraph();
						$image = get_field('hero', $post);
						$cat = wp_get_post_terms($post->ID, $custom_tax);
						$link = get_permalink();
        				break;

            		default:
            			$link = get_permalink();
            			break;

            	} ?>

				<li>
					<h2><a href="<?php echo $link; ?>"><?php echo strtoupper(get_the_title());  ?></a></h2>
					<div class="result-body-wrap"><?php echo $body; ?><a class="red" href="<?php echo $link; ?>"> read more</a></div>
					<div class="break"></div>
				</li>

            <?php } ?>

            </ul>

           <?php paginate_links(); ?>

        <?php } ?>

    </div>
  </div>
</div>
<?php get_footer(); ?>
