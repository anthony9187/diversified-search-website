<?php

//initiate a primary navigation
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'My_Nav' ),
) );

//remove default post type
add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
	remove_menu_page('edit.php');
}

//remove default wysiwyg editor from pages
add_action('init', 'init_remove_support',100);
function init_remove_support(){
    $post_type = 'page';
    remove_post_type_support( $post_type, 'editor');
}

// Allow .vcf files to upload to the media library
 add_filter('upload_mimes', 'custom_upload_mimes');
 
 function custom_upload_mimes ( $existing_mimes=array() ){
 	// add your extension to the array
 	$existing_mimes['vcf'] = 'text/x-vcard'; return $existing_mimes;
 }

//enqueue styles and scripts
 $enqueue_dir = require get_template_directory() . '/func/enqueue.php';
if (file_exists($enqueue_dir)) {
	require $enqueue_dir;
};

add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'news', 'success-stories', 'insights');
    $query->set('post_type',$post_type);
    return $query;
    }
}

// flush_rewrite_rules();

$pagination_dir = require get_template_directory() . '/func/pagination.php';
if (file_exists($pagination_dir)) {
    require $pagination_dir;
};

// permalink rewrites for CPT archives
$permalinks_dir = require get_template_directory() . '/func/permalinks.php';
if (file_exists($permalinks_dir)) {
    require $permalinks_dir;
};

// Custom Taxonomy
 $custom_taxonomy_dir = require get_template_directory() . '/func/taxonomies.php';
if (file_exists($custom_taxonomy_dir)) {
    require $custom_taxonomy_dir;
};

// Custom Post Types
 $custom_post_types_dir = require get_template_directory() . '/func/post-types.php';
if (file_exists($custom_post_types_dir)) {
	require $custom_post_types_dir;
};

// Customizer Additions
$customizer_dir = require get_template_directory() . '/func/customizer.php';
if (file_exists($customizer_dir)) {
	require $customizer_dir;
};

// Options Additions
$options_dir = require get_template_directory() . '/func/options.php';
if (file_exists($options_dir)) {
    require $options_dir;
};

// Hide Featured Image In Admin Panel
add_action( 'admin_menu', 'remove_thumbnail_box' );

function remove_thumbnail_box() {
    remove_meta_box( 'postimagediv', 'page', 'side' );
}

// set CPT posts per page to 6

function custom_posts_per_page( $query ) {
    if (!is_admin()) {
        if ( $query->is_archive('insights') || $query->is_archive('success-stories') ) {
            set_query_var('posts_per_page', 6);
        }
    }
}
add_action( 'pre_get_posts', 'custom_posts_per_page' );

// get first paragraph of custom post type
function get_first_paragraph(){
    global $post;
    $str = wpautop( get_field('body') );
    $str = substr( $str, 0, strpos( $str, '</p>' ) + 4 );
    $str = strip_tags($str, '<a><strong><em>');
    return '<p>' . $str . '</p>';
}

?>