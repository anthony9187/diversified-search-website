<?php
get_header(); ?>
<div class="container">
	<div class="page-not-found bg-white">
		<div class="body">
			<h4><?php _e( 'I’m sorry, it looks like we’ve taken a wrong turn.'); ?></h5>
			<h5><?php _e( 'Please use the navigation to better help you find what you’re looking for.' ); ?></h5>
		</div>
	</div>
</div>
<?php
//get_sidebar();
get_footer();
?>