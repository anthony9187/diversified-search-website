</div><?php //main-wrap-closer ?>

<?php global $post; $post_slug=$post->post_name; ?>

<footer class="<?php echo $post_slug; ?>">
	
	<div class="search-footer<?php if (is_front_page()) {echo ' fade-home';} ?>"><a id="search-button" href="">SEARCH FOR CONSULTANTS, INDUSTRIES, AND INSIGHTS <svg class="footer-search-svg"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-play"></use></svg></a></div>

	<a class="submit white<?php if (is_front_page()) {echo ' fade-home';} ?>" href="https://searchlight.cluen.com/E5/Login.aspx?URLKey=nqqzejyu" target="_blank"><img src="<?php echo get_template_directory_uri() . '/img/cv-icon.png'; ?>" alt="">SHARE YOUR RESUME WITH US</a>
	
	<?php if( have_rows('partner_logos', 'option') ): ?>

	    <?php while ( have_rows('partner_logos', 'option') ) : the_row(); ?>

	        <a href="//<?php the_sub_field('partner_link', 'option'); ?>" target="_blank" class="<?php if (is_front_page()) {echo 'fade-home';} ?>"><img src="<?php echo get_sub_field('partner_logo', 'option')['url']; ?>" alt="<?php the_sub_field('partner_name', 'option'); ?>"></a>
	        
	    <?php endwhile; ?>

	<?php endif; ?>
	

</footer>
<?php wp_footer(); ?>
</body>
</html>