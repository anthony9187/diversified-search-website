<?php
/**
 * Template Name: Our Story
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<div id="main" class="site-main" role="main">

			<?php include 'inc/section-aspot.php'; ?>
			
			<div class="body-wrapper assessment">
				
				<div class="left-column">
					<p class="assessment-header"><?php the_field('header'); ?></p>
					<p><?php the_field('body_1'); ?></p>
				</div>
				<div class="img-container">
					<img class="img-round" src="<?php echo get_field('image')['sizes']['thumbnail']; ?>" alt="">
					<a class="cta-button" href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ) . '#alison-byers'; ?>"><?php the_field('button_text') ?></a>
				</div>
				<div class="bottom-text">
					<p><?php the_field('body_2'); ?></p>
					<p><?php the_field('body_3'); ?></p>
				</div>

			</div>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>