<?php get_header() ?>

<div class="container single bg-gray">
	<div class="single-wrapper">
		<?php while ( have_posts() ) { the_post(); ?>

			<?php 
				$custom_tax = 'category-' . $post_type;
				$image = get_field('hero');
				$title = ucwords(get_the_title());
				$this_type = get_post_type();
				$post_obj = get_post_type_object($this_type);
				$author = ucwords(get_the_author());
				$this_date = get_the_date();
				$cat = wp_get_post_terms($post->ID, $custom_tax);
				$subtitle = get_field('subtitle');

			?>
			<div class="header-container">
				<?php if ($image !== false) { ?> 
					<div class="title-img">
						<img src="<?php echo $image['sizes']['large']; ?>" alt="">
					</div>
				<?php } ?>

				<div class="title-container">

					<h1><?php echo $title; ?></h1>

					<?php if ($post_type === 'insights') { ?>
						<?php $authors = get_field('authors'); ?>
						<?php if (!empty($authors)) { ?>
							<?php foreach ($authors as $a) { ?>
								<div class="author">
									<?php 
										$pic = get_field('picture', $a);
										if ($pic == false) { 
											$pic = get_template_directory_uri() . '/img/ds-logo.png';
										} else { 
											$pic = $pic['sizes']['thumbnail'];
										};
									?>
									<a href="<?php echo get_permalink( get_page_by_title( 'Our People' ) ) . '#' . $a->post_name ; ?>">
										<img class="img-round" src="<?php echo $pic; ?>" alt="author">
										<p><span class="bold"><?php echo $a->post_title; ?></span>
										<?php the_field('title', $a); ?></p>
									</a>
								</div>
							<?php } ?>
						<?php } ?>
					<?php } ?>

				</div>
			</div>

			<div class="date"><?php the_date(); ?></div>

			<?php if (!empty($cat) && ($post_type !== 'news')) { ?>
				<a class="red category" href="<?php echo get_post_type_archive_link( $post_type ) . $cat[0]->slug; ?>"><?php echo $cat[0]->name; ?></a>
			<?php } ?>

			<?php if (!empty($subtitle)) { ?>
				<h2><?php echo $subtitle; ?></h2>
			<?php } ?>

			<div class="single-body">
				<p><?php echo get_field('body'); ?></p>
			</div>

			<a class="cta-button back white" href="<?php echo get_site_url() . '/' . $this_type ?>">
				<svg class=""><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-arrow-left"></use></svg>
				<span class="white">Back To <?php echo $post_obj->label; ?></span>
			</a>

		<?php } ?>	
	</div>
</div>
<?php get_footer() ?>

