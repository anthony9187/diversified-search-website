<?php
/**
 * Template Name: Our People Search Results Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

$page_name = str_replace(' ', '-', ( strtolower( get_the_title() ) ) );

get_header(); ?>
	
	<div id="primary" class="content-area">

		<div id="main" class="site-main" role="main">
			
			<div class="nav-clear"></div>
			
			<?php include 'inc/section-toolbar.php' ?>
			
			<?php include 'inc/list/section-our-people-search.php' ?>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>