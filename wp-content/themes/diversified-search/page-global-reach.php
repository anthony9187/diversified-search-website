<?php
/**
 * Template Name: Global Reach
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<div id="main" class="site-main bg-gray" role="main">

			<?php include 'inc/section-aspot.php'; ?>
			
			<div class="body-wrapper global-reach">
				
				<p class="center"><?php the_field('subheader', false, false); ?></p>

				<div class="partner-img">
					<a href="<?php the_field('partner_image_link', false, false) ?>" target="_blank"><img src="<?php echo get_field('partner_image')['url']; ?>" alt=""></a>
				</div>

				<div class="partner-info">
					<div class="info-left">
						<h5 class="black"><?php the_field('content_left_title', false, false); ?></h5>
						<p><?php the_field('content_left', false, false); ?></p>
					</div>
					<div><?php the_field('content_right'); ?></div>
				</div>

				<div class="services-quote-block section-quote">
					<p><svg class="svg-quote open">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-open"></use>
					</svg><?php the_field('quote_block', false, false); ?><svg class="svg-quote close">
						<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-close"></use>
					</svg></p>
					<p class="attribution"><?php the_field('quote_attribution', false, false); ?></p>
				</div>

				<div class="partner-services bg-gray">

					<div class="partner-services-left">

						<h3><?php echo strtoupper( get_field('service_left_header') ); ?></h3>
						
						<p><?php echo get_field('service_left_sub_header', false, false); ?></p>

						<div class="service">
							<span><?php echo strtoupper(get_field('service_left_one_name')); ?></span>
							<p><?php the_field('service_left_one_description', false, false); ?></p>	
						</div>

						<div class="service">
							<span><?php echo strtoupper(get_field('service_left_two_name')); ?></span>
							<p><?php the_field('service_left_two_description', false, false); ?></p>
						</div>

					</div>

					<div class="partner-services-right">
						<h4><?php the_field('service_right_header', false, false); ?></h4>
						<?php the_field('service_right_body'); ?>
					</div>

				</div>

				<div class="partner-offices-header">
					<div class="title-container"><h3><?php echo strtoupper(get_field('int_offices_title')); ?></h3></div>
					<img src="<?php echo get_field('title_background_img')['url']; ?>" alt="">
				</div>

				<div class="partner-offices bg-white">

					<div class="sub-title"><?php the_field('int_offices_sub_title', false, false); ?></div>
					
					<?php $subtitle_image = get_field('int_offices_sub_title_image'); ?>
					<?php if (!empty($subtitle_image)) { ?>
						<img src="<?php echo $subtitle_image['sizes']['large']; ?>" alt="">
					<?php } ?>

					<div class="int-location-container">

						<?php $int_regions = get_field('int_regions'); ?>
						<?php foreach ($int_regions as $region) { ?>

							<div class="int-location" id="<?php echo $region['int_region_name'] ?>">
								
								<div class="int-region-name">
									<h6 class="red"><?php echo $region['int_region_name'] ?></h6>
								</div>
								
								<?php $int_locations = $region['int_locations'];  ?>

								<ul class="int-location-list">
									<?php foreach ($int_locations as $location) { ?>
										<li>
											<span class="bold"><?php echo $location['int_country']; ?>:</span>
											<span><?php echo $location['int_city']; ?></span>
										</li>
									<?php } ?>
								</ul>

							</div>

						<?php } ?>
					</div>
				</div>
			</div>
		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>