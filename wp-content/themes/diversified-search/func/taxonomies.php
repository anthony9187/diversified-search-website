<?php 

add_action( 'init', 'custom_taxonomy', 0 );

function custom_taxonomy() {

	register_taxonomy( 'type',
						'practices', 
						array( 
							'hierarchical' => true, 
							'label' => 'Practice Type',
							'show_admin_column' => true,
							'query_var' => true, 
							'rewrite' => true 
						) 
	);

	register_taxonomy( 'category-insights',
						'insights',
						array(
							'hierarchical' => true, 
							'label' => 'Category',
							'show_admin_column' => true,
							'query_var' => true, 
							'rewrite' => true 
						)
	);

	// removed news categories
	// register_taxonomy( 'category-news',
	// 					'news',
	// 					array(
	// 						'hierarchical' => true, 
	// 						'label' => 'Category',
	// 						'show_admin_column' => true,
	// 						'query_var' => true, 
	// 						'rewrite' => true 
	// 					)
	// );
	register_taxonomy( 'category-success-stories',
						'success-stories',
						array(
							'hierarchical' => true, 
							'label' => 'Category',
							'show_admin_column' => true,
							'query_var' => true, 
							'rewrite' => true 
						)
	);
}
?>