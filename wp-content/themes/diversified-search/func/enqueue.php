<?php
function enqueue_custom() {

    /** REGISTER css/screen.cs **/
    // wp_register_style( 'screen-style', THEME_DIR . '/css_path/screen.css', array(), '1', 'all' );
    // wp_enqueue_style( 'screen-style' );
    wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, 'all' );
    wp_enqueue_style( 'animatecss', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css', false, 'all' );
    wp_enqueue_style( 'wpb-google-fonts', 'http://fonts.googleapis.com/css?family=Source+Sans+Pro:300italic,400italic,700italic,400,900,700,300', false );

    wp_enqueue_style( 'bozemanlight', get_template_directory_uri() . '/vendor/fonts/bozeman_light/stylesheet.css' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css' );

	wp_deregister_script('jquery');

    wp_enqueue_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js", array(), false, true);
    wp_enqueue_script('plugin', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), false, true);
	wp_enqueue_script('scripts', get_template_directory_uri() . '/js/scripts-min.js', array( 'jquery' ), false, true);

};

add_action( 'wp_enqueue_scripts', 'enqueue_custom' );

?>