<?php

add_action('init', 'custom_post_types');

function custom_post_types() {

	// Object CPTs

	register_post_type( 'People',
		array(
			'labels' => array(
				'name' 			=> __( 'People' ),
				'singular_name' => __( 'Person' ),
				'add_new' 		=> __( 'Add New Person' ),
				'add_new_item' 	=> __( 'Add New Person' ),
				'new_item' 		=> __( 'New Person' ),
				'edit_item' 	=> __( 'Edit Person' ),
				'view_item' 	=> __( 'View Person' )
			),
			'hierarchical' 		=> false,
			'menu_icon'   		=> 'dashicons-businessman',
			'menu_position'		=> 20,
			'public'			=> true,
			'show_in_nav_menus' => true,
			'show_ui'			=> true,
			'supports' 			=> array( 'title', 'revisions' ),
			'rewrite'			=> array( 'slug' => 'our-people' ),
		)
	);

	register_post_type( 'Practices',
		array(
			'labels' => array(
				'name' 			=> __( 'Practices' ),
				'singular_name' => __( 'Practice' ),
				'add_new' 		=> __( 'Add New Practice' ),
				'add_new_item' 	=> __( 'Add New Practice' ),
				'new_item' 		=> __( 'New Practice' ),
				'edit_item' 	=> __( 'Edit Practice' ),
				'view_item' 	=> __( 'View Practice' )
			),
			'hierarchical' 		=> false,
			'menu_icon'   		=> 'dashicons-welcome-learn-more',
			'menu_position'		=> 20,
			'public'			=> true,
			'show_in_nav_menus' => true,
			'show_ui'			=> true,
			'supports' 			=> array( 'title', 'revisions' ),
			'has_archive' 		=> false
		)
	);

	register_post_type( 'Offices',
		array(
			'labels' => array(
				'name' 			=> __( 'Offices' ),
				'singular_name' => __( 'Office' ),
				'add_new' 		=> __( 'Add New Office' ),
				'add_new_item' 	=> __( 'Add New Office' ),
				'new_item' 		=> __( 'New Office' ),
				'edit_item' 	=> __( 'Edit Office' ),
				'view_item' 	=> __( 'View Office' )
			),
			'hierarchical' 		=> false,
			'menu_icon'   		=> 'dashicons-admin-multisite',
			'menu_position'		=> 20,
			'public'			=> true,
			'show_in_nav_menus' => true,
			'show_ui'			=> true,
			'supports' 			=> array( 'title', 'revisions' ),
			'has_archive' 		=> false
		)
	);

	// Blog CPTs

	register_post_type( 'News',
		array(
			'labels' => array(
				'name' 			=> __( 'News' ),
				'singular_name' => __( 'News' ),
				'add_new' 		=> __( 'Add New News' ),
				'add_new_item' 	=> __( 'Add New News' ),
				'new_item' 		=> __( 'New News' ),
				'edit_item' 	=> __( 'Edit News' ),
				'view_item' 	=> __( 'View News' )
			),
			'hierarchical' 		=> false,
			'menu_icon'   		=> 'dashicons-media-text',
			'menu_position'		=> 15,
			'public'			=> true,
			'show_in_nav_menus' => true,
			'show_ui'			=> true,
			'supports' 			=> array( 'title', 'author', 'excerpt', 'revisions' ),
			'rewrite'			=> array( 'slug' => 'news', 'with_front' => false ),
			'has_archive' 		=> 'news'
		)
	);

	register_post_type( 'Insights',
		array(
			'labels' => array(
				'name' 			=> __( 'Insights' ),
				'singular_name' => __( 'Insight' ),
				'add_new' 		=> __( 'Add New Insight' ),
				'add_new_item' 	=> __( 'Add New Insight' ),
				'new_item' 		=> __( 'New Insight' ),
				'edit_item' 	=> __( 'Edit Insight' ),
				'view_item' 	=> __( 'View Insight' )
			),
			'hierarchical' 		=> false,
			'menu_icon'   		=> 'dashicons-visibility',
			'menu_position'		=> 15,
			'public'			=> true,
			'show_in_nav_menus' => true,
			'show_ui'			=> true,
			'supports' 			=> array( 'title', 'excerpt', 'revisions' ),
			'rewrite' 			=> array( 'slug' => 'insights/%category-insights%', 'with_front' => false ),
			'has_archive' 		=> 'insights'
		)
	);

		register_post_type( 'Success-Stories',
		array(
			'labels' => array(
				'name' 			=> __( 'Success Stories' ),
				'singular_name' => __( 'Success Story' ),
				'add_new' 		=> __( 'Add New Success Stories' ),
				'add_new_item' 	=> __( 'Add New Success Stories' ),
				'new_item' 		=> __( 'New Success Stories' ),
				'edit_item' 	=> __( 'Edit Success Stories' ),
				'view_item' 	=> __( 'View Success Stories' )
			),
			'hierarchical' 		=> false,
			'menu_icon'   		=> 'dashicons-awards',
			'menu_position'		=> 15,
			'public'			=> true,
			'show_in_nav_menus' => true,
			'show_ui'			=> true,
			'supports' 			=> array( 'title', 'author', 'excerpt', 'revisions' ),
			'rewrite' 			=> array( 'slug' => 'success-stories/%category-success-stories%', 'with_front' => false ),
			'has_archive' 		=> 'success-stories'
		)
	);
}
?>