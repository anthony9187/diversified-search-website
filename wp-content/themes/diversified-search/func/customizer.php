<?php

add_action( 'customize_register', 'customizer_register' );

function customizer_register( $wp_customize ) {

	// panels
	$wp_customize->add_panel( 'nav', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Navigation', 'textdomain' ),
	    'description' => __( 'Description of what this panel does.', 'textdomain' ),
	) );

	// sections
	$wp_customize->add_section( 'nav_footer_section', array(
	    'priority' 	=> 10,
	    'panel' => 'nav',
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Navigation Footer Text', 'textdomain' ),
	    'description' => '',
	));

	$wp_customize->add_section( 'nav_social_section', array(
	    'priority' => 1,
	    'panel' => 'nav',
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Navigation Social Urls', 'textdomain' ),
	    'description' => '',
	));

	$wp_customize->add_section( 'nav_address_section', array(
	    'priority' => 1,
	    'panel' => 'nav',
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Address', 'textdomain' ),
	    'description' => '',
	));


	// customizer fields //

	// social urls
	$links = array ('linkedin', 'facebook', 'twitter');
	foreach ($links as $link) {
		$wp_customize->add_setting( 'nav_social_' . $link, array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport' => '',
			'sanitize_callback' => 'esc_url',
		) );
		$wp_customize->add_control( 'nav_social_' . $link, array(
		    'type' => 'url',
		    'priority' => 9,
		    'section' => 'nav_social_section',
		    'label' => __( $link .' url', 'textdomain' ),
		    'description' => '',
		) );
	}

	// footer
	$wp_customize->add_setting( 'navigation_footer', array(
		'default' => '',
		'type' => 'theme_mod',
		'capability' => 'edit_theme_options',
		'transport' => '',
		'sanitize_callback' => 'esc_textarea',
	) );

	// address
	$fields = array ('street', 'cityStateZip', 'country');
	foreach ($fields as $field) {
		$wp_customize->add_setting( 'nav_address_' . $field, array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'transport' => '',
			'sanitize_callback' => 'esc_textarea',
		) );
		$wp_customize->add_control( 'nav_address_' . $field, array(
		    'priority' => 9,
		    'section' => 'nav_address_section',
		    'label' => __( $field, 'textdomain' ),
		    'description' => '',
		) );
	}

}
?>