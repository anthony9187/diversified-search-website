<?php

add_filter( 'post_type_link', 'wpa_insights_permalinks', 1, 2 );
add_filter( 'post_type_link', 'wpa_success_stories_permalinks', 1, 2 );

function wpa_insights_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'insights' ){
        $terms = wp_get_object_terms( $post->ID, 'category-insights' );
        if( $terms ){
            return str_replace( '%category-insights%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}

function wpa_success_stories_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'success-stories' ){
        $terms = wp_get_object_terms( $post->ID, 'category-success-stories' );
        if( $terms ){
            return str_replace( '%category-success-stories%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}

// removed news categories rewrite

// add_filter( 'post_type_link', 'wpa_news_permalinks', 1, 2 );

function wpa_news_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'news' ){
        $terms = wp_get_object_terms( $post->ID, 'category-news' );
        if( $terms ){
            return str_replace( '%category-news%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;

}

?>