<?php  


add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'previous_posts_link_attributes');

function previous_posts_link_attributes() {
    return 'class="cta-button page-button page-prev"';
}
function next_posts_link_attributes() {
    return 'class="cta-button page-button page-next"';
}


?>