<!doctype html>
<html <?php if (get_page_template_slug() == 'page-card.php') {echo 'class="gradient"';}?>>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width">

	<title><?php
		if( get_post_type() === 'page' ) {
			$page_name = the_title_attribute();
			echo ucfirst($page_name) . ' | ';
		} elseif ( is_single() ) {
			the_title();
		} elseif ( is_tax() ) {
			$page_type = get_queried_object();
			$page_name = $page_type->name;
			echo ucfirst($page_name) . ' | ';
		} else {
			$page_type = get_queried_object();
			$page_name = $page_type->labels->name;
			// var_dump($page_type);
			echo ucfirst($page_name) . ' | ';
		} 
		echo bloginfo( 'name' ); 
	?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" />
	<?php wp_head(); ?>
</head>

<body <?php //body_class(); ?>>
<header>
<?php $current_page = get_page_template_slug( $post->ID ); ?>
<div class="nav-container<?php if ( ($current_page == 'page-home.php') ) { echo ' transparent'; } ?>">
	<a href="<?php echo get_site_url(); ?>"><svg class="ds-logo-svg<?php if (is_front_page()) {echo ' fade-home';} ?>"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#DS-logo"></use></svg><svg class="ds-words-svg<?php if (is_front_page()) {echo ' fade-home';} ?>"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#DS-words"></use></svg></a>
	<div class="nav-button <?php if (is_front_page()) {echo 'fade-home';} ?>">
		<p class="white">MENU</p>
	    <div class="nav-icon">
	       <div></div>
	    </div>
	</div>
</div>

<?php include 'inc/nav-menu.php'; ?>

</header>
<div class="main-wrap">
<div class="search-popup-wrap">
	<a href="#" class="button-close search"><svg class="svg-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-ui-cancel"></use></svg></a>
	<?php get_search_form(); ?>
</div>
<!--main-wrap-opener-->