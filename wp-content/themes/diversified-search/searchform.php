<?php
/**
* Template for displaying search forms
*
*/
?>

<div class="search-form">
	<label class="screen-reader-text srt-only" for="s">Search:</label>
	<form id="searchform" method="get" action="/index.php">
		<input type="text" name="s" id="s" placeholder="SEARCH..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'SEARCH...'" /><svg class="svg-search"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#icon-search"></use></svg>
	</form>
</div>
