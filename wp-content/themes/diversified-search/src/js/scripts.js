$( document ).ready(function() {
	
	svg4everybody();

	cardHeight();
	// scrollToCard();

	dropdownMenu();
	navButtons();


	homeAnimations();
	categoryAnimations();

	searchMenu();
});
window.onload = function(){
	scrollToCard();
}

$('.nav-container').headroom();

//region select
$('a.dropdown-select').click(function(e){
	region = $(this).attr('data-region').replace(/ /g, '-').toLowerCase();
	
	$('.location-wrap').fadeOut(500);
	$('#' + region).fadeIn(500);
	$('html,body').animate( { scrollTop: $('#' + region).offset().top -200 }, 1000 );

})

function searchMenu(){
	$('#search-button').click(function(e){
		e.preventDefault();

		$('.search-popup-wrap').fadeIn(300);
	})
	$('.button-close.search').click(function(){ $('.search-popup-wrap').fadeOut(300); })
}

function categoryAnimations(){

	url = window.location.href.split('/');

	if ((url[3] === 'about-us') || (url[3] === 'our-expertise')) {

		if ( navigator.userAgent.indexOf('MSIE') !==-1 || navigator.appVersion.indexOf('Trident/') > 0 ) {

			if (url[3] === 'about-us') {
				adjust = 70;
			} else if (url[3] === 'our-expertise') {
				adjust = 20;
			};
				currentSize = parseInt($('.svg-container svg').css('font-size'), 10);
				newSize = currentSize + adjust;
				$('.svg-container svg').css('font-size', newSize+'px');	
		}
		
		trace = anime({
			targets: '.svg-container svg text',
			strokeDashoffset: 0,
			easing: 'easeInOutSine',
			direction: 'alternate',
			duration: 3500
		})
		fill = anime({
			targets: '.svg-container svg text',
			fill: '#fff',
			easing: 'easeInOutSine',
			fillOpacity: 1,
			delay: 2000,
			duration: 1000
		})
	}
}

function homeAnimations(){
	
	if (window.innerWidth > 1023){


			$('.home-overlay').delay(2000).fadeTo(2000, 1);
			$('footer').delay(4000).fadeTo(100, 1);

		$('[data-text="home"]').textillate({
			initialDelay: 2000,
			inEffects: ['hidden'],
			in: {
				effect: 'slideInUp',
				sync: true,
			},
			type: 'word',
		});
		$('.fade-home').delay(4500).fadeTo(2000, 1);
	} else {
		$('[data-text="home"]').textillate({
			initialDelay: 500,
			inEffects: ['hidden'],
			in: {
				effect: 'slideInUp',
				sync: true,
			},
			type: 'word',
		});
		$('footer .fade-home').delay(2000).fadeTo(2000, 1);
		$('.fade-home').delay(1500).fadeTo(2000, 1);
		}
}

//srcoll to and open card
function scrollToCard(){
	url = window.location.href.split('/');

		switch(url[3]) {
		case 'about-us':
			page = 'office';
		break;

		case 'our-people':
			page = 'person';
		break;

		case 'our-expertise':
			page = 'item';
		break;

		default:
			return;
		break;
	};
	
	card_id = window.location.href.slice(window.location.href.indexOf('#'));
	
	if ((card_id == '/') || (card_id == '#')) {
		return;
	}
	card_name = '.'+page+'-thumb[data-id="'+card_id.substr(1)+'"]';
	
	$(card_name).trigger('click');
}

function cardHeight(){
	url = window.location.href.split('/');
	quotes = false;

		switch(url[3]) {

		case 'about-us':
			if (url[4] === 'leadership') {
				card = 'person';
				thumb_height = 367;
				quotes = true;
			} else if (url[4] === 'us-offices') {
				card = 'office';
				thumb_height = 354;
			} else {
				return;
			}
		break;

		case 'our-people':
		case 'our-people-search-results':
			card = 'person';
			thumb_height = 367;
		break;

		case 'our-expertise':
			card = 'item';
			thumb_height = 350;
		break;

		default:
			return;
		break;
	};

	$('.'+card+'-thumb').click(function(e){
		e.preventDefault();

		if ( $(this).hasClass('active') ) {
			$('.'+card+'-card').slideUp(500);
			$('.'+card).delay(300).animate({ height: thumb_height }, 1000);
			$('.'+card+'-thumb').removeClass('active');
			if( quotes == true ){
				$('.leadership-quote').hide();
				$('.leadership-quote.default').show();
			}

		} else {
			$('.'+card+'-card').hide();
			$('.'+card).delay(300).css('height', thumb_height);
			$('.'+card+'-thumb').removeClass('active');
			if( quotes == true ){
				$('.leadership-quote').hide();
			}

			current = $(this).attr('data-id');

			if( quotes == true ){
				$('.leadership-quote').hide();
				$('.leadership-quote[data-id="'+current+'"]').fadeIn(500);
			}

			$(this).addClass('active');
			height = $('.'+card+'-card[data-id="'+current+'"]').outerHeight() + thumb_height + 50;
			$(this).parent().css('height', height);
			
			$('.'+card+'-card[data-id="'+current+'"]').delay(300).slideDown(500);

			$("html, body").animate({scrollTop: $(this).offset().top +250 }, 1000);
		}
	})

	$('.button-'+card+'-close').click(function(e){
		e.preventDefault();
		$('.'+card+'-card').slideUp();
		$('.'+card).delay(300).animate({ height: thumb_height }, 1000);
		$('.'+card+'-thumb').removeClass('active');
		if( quotes == true ){
			$('.leadership-quote').hide();
			$('.leadership-quote.default').fadeIn(500);
		}
	})
}

function navButtons(){
	$('.nav-button').click(function(){
		$('.nav-slider').addClass('open');
	});
	$('.button-close.home').click(function(){ $(this).parent().removeClass('open');});
};

function dropdownMenu(){
	$('.dropdown-btn').click(function(e){
		e.preventDefault();

		open = $('.dropdown-content.open').data('cat');

		if ($(this).data('cat') == open ) {
			$('.dropdown-content').removeClass('open').slideUp();
		} else {
			$('.dropdown-content').removeClass('open').slideUp();
			cat = $(this).attr('data-cat');
			$('.dropdown-content[data-cat="'+cat+'"]').toggleClass('open').slideToggle();
		}
	})
	$(document).click(function(e) {

	if (!$('.dropdown-btn').is(e.target) ) {
		$('.dropdown-content').removeClass('open').slideUp();
	}
});
}