/* UNIVERSAL */
/**************/
h1, h2, h3, h4, h5, h6 {
	color: $blue;
	font-family: $font1;
}

span, p, div, a {
	color: black;
	font-family: $font1;
}
h1 {
	
}
.bold {
	font-weight: bold;
}
.white {
	color: white;
}
.black {
	color: black;
}
.red {
	color: $red;
}

a, a:link, a:visited, a:active {
	font-weight: 400;
	text-decoration: none;
	color: $red;
	@include buttonTransition()
	&:hover {
		color: $blue;
	}
}

a.blue, a.blue:link, a.blue:visited, a.blue:active {
	font-weight: 400;
	text-decoration: none;
	color: $blue;
	@include buttonTransition()
	&:hover {
		color: $red;
	}
}

a.white, a.white:link, a.white:visited, a.white:active {
	font-weight: 400;
	text-decoration: none;
	color: white;
	@include buttonTransition()
	&:hover {
		color: $red;
	}
}

a.cta-button {
	background-color: $red;
	padding: 5px 20px;
	color: white;
	&:hover {
		background-color: $blue;
		color: white;
	}
}

a.link-button {
	color: $dark-blue;
	background-color: white;
	padding: 10px 20px;
	@include buttonTransition()

	@include breakFirst(md) {
		padding: 10px 20px;
		margin: 0 10px;
	}
	&:hover {
		background-color: $red;
		color: white;
	}
}
body {
   height: 100%;
   width: 100%;
   clear: both;
}

html * {
	line-height: 1.4;
}

html {
	background-color: $dark-blue;
}

// sticky footer
body {
	display: flex;
	flex-direction: column;
}
.content-area {
	// &.fullscreen {
	// 	min-height: 750px;
	// }
	flex: 1 0 auto;
}

.container {
	margin-top: 50px;
}

.main-wrap {
	//margin: 80px 0;
	margin: 0;
}

.break {
	border-bottom: 1px solid black;
}

.srt-only {
	border: 0;
	clip: rect(0 0 0 0);
	height: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	width: 1px;
}

.gradient {
	background: $light-blue; /* For browsers that do not support gradients */
	/* For Safari 5.1 to 6.0 */
	background: -webkit-radial-gradient($gradient);
	/* For Opera 11.1 to 12.0 */
	background: -o-radial-gradient($gradient);
	/* For Fx 3.6 to 15 */
	background: -moz-radial-gradient($gradient);
	/* Standard syntax */
	background: radial-gradient($gradient);
}

// #bg_pattern { 
// 	background-image: url(img/texture.png);
// 	position: fixed; 
// 	opacity: 0.8; 
// 	left: 0px; 
// 	top: 0px; 
// 	width: 100%; 
// 	height: 100%; 
// 	z-index: -1;
// }

.grayscale {
    filter: grayscale(100%);
}

.grayscale-hov {
    filter: grayscale(100%);
    @include buttonTransition();
    &:hover {
	    filter: grayscale(0);
    }
}

.director {
	.grayscale-hov {
	    filter: grayscale(100%);
	    @include buttonTransition();
	}
	 &:hover {
	 	.grayscale-hov {
	    	filter: grayscale(0);
		}
    }
}

.bg-gray {
	background-color: $bg-grey;
}

.bg-white {
	background-color: white;
}

.subheader {
	padding: 40px 12% 0;
	text-align: center;
	font-weight: 400;
	background-color: $bg-grey;
	@include breakFirst(md) {
		padding: 80px 12%;
	}
}
.svg-close {
	color: white;
	height: 40px;
	width: 40px;
	z-index: 999;
	@include buttonTransition()
	&:hover {
		color: $red;
	}
}
.linkedin-svg {
	height: 25px;
	width: 25px;
	fill: $blue;
}
.break {
	display: block;
	border: 1px solid $blue;
	@include breakFirst(md) {
		width: 200px;
	}
}
.nav-clear {
	margin-top: 90px;
}
.img-round {
	overflow: hidden;
	border-radius: 50%;
} 
.img-square {
	overflow: hidden;
}

.section-quote {
		background-color: $red;
		color: white;
		text-align: center;
		padding-bottom: 40px;

		.quote-body {
			svg, p {
				display: inline-block;	
			}
		}
		.svg-quote {
			fill: white;
			height: 70px;
			width: 120px;
			&.close {
				margin-bottom: -45px;
			}
		}
		p {
			text-align: center;
			color: white;
			padding: 25px 0;
			font-size: 2em;
			font-style: italic;
			font-weight: 100;
			@include breakFirst(sm) {
				font-size: 2em;
			}
		}
		.attribution {
			display: block !important;
			text-align: center;
		}
	}

.quote {
	&:before {
		content: "";
		background: url('img/quote-open.svg') no-repeat;
		width: 20px;
		height: 20px;
		position: absolute;
	}

	&:after {
		content: "";
		background:  url('img/quote-close.svg') no-repeat;
		width: 20px;
		height: 20px;
		position: absolute;
	}
}
