<?php
/**
 * Template Name: Blog List Template
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<div id="main" class="site-main bg-gray" role="main">
			
			<?php include 'inc/section-aspot.php'; ?>

			<div class="container">

				<?php

					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

					$wpb_all_query = new WP_Query(array('post_type' => $post_type, 
														'posts_per_page' => 6,
														'paged' => $paged
														)
												); 
					
					if ( $wpb_all_query->have_posts() ) : 
				?>

					<div class="post-list-wrap">

						<!-- the loop -->
						<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

							<?php 
								$body = substr(get_field('body', false, false), 0, 400) . '...';
								$image = get_field('hero');
								

								if ($image == false) { 
									$image = get_template_directory_uri() . '/img/ds-logo.png';
								} else {
									$image = $image['sizes']['medium'];
								}
							?>
							<div>
								<div class="post-preview">
									<div class="post-preview-img">
										<img src="<?php echo $image; ?>" alt="">
									</div>
									<div class="post-preview-text">
										<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
										<p><?php echo $body; ?><a href="<?php the_permalink(); ?>">Read More</a></p>
									</div>
								</div>
								<div class="break"></div>
							</div>
						<?php endwhile; ?>

						<?php // end of the loop ?>

						<?php if($wpb_all_query->max_num_pages > 1) : ?>

							<?php previous_posts_link('Newer Entries'); ?>
							
							<?php next_posts_link( 'Older Entries' ); ?>

						<?php endif; ?>

						

					</div>

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>