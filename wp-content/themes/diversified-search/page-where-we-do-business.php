<?php
/**
 * Template Name: Where We Do Business
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

$page_name = str_replace(' ', '-', ( strtolower( get_the_title() ) ) );
$toolbar = get_field('toolbar');

get_header(); ?>

	<div id="primary" class="content-area-business">

		<div id="main" class="site-main" role="main">

			<?php include 'inc/section-aspot.php'; ?>
			
			<?php include 'inc/section-toolbar.php' ?>
			
			<div class="subheader">
				<p><?php echo get_field('subheader'); ?></p>
			</div>

			<div class="location-container">
				<?php $regions = get_field('region'); ?>
				
				<?php foreach ($regions as $region) { ?>
				<?php $region_slug = strtolower(str_replace( ' ', '-', $region['region_name'])); ?>
				<?php //var_dump($region_slug); ?>
					<div class="location-wrap" id="<?php echo $region_slug ?>">
						<div class="region-name">
							<h5 class="black"><span><?php echo $region['region_name'] ?></span></h5>
						</div>
						<?php $locations = $region['locations'];  ?>
						<ul class="location-list">
							<?php foreach ($locations as $location) { ?>
							
							<li><p><?php echo $location['location_name']; ?></p></li>
						<?php } ?>
						</ul>
					</div>
				<?php } ?>
			</div>

			<?php if ( get_field('map') ){ ?>
				<?php $map = get_field('map'); ?>
				<div class="map-container">
					<img src="<?php echo $map['url']; ?>" alt="">
				</div>
			<?php } ?>

		</div><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>