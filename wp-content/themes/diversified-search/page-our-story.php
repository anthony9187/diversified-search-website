<?php
/**
 * Template Name: Our Story
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Custom_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<div id="main" class="site-main bg-gray" role="main">

			<?php include 'inc/section-aspot.php'; ?>
			
			<div class="body-wrapper our-story">
				
				<div class="section-1">

					<div class="section-1-text">
						<p class="header"><?php the_field('section_one_headline'); ?></p>
						<p class="body"><?php the_field('section_one_body', false, false); ?></p>
					</div>

					<figure>
						<img src="<?php echo get_field('section_one_image')['url']; ?>">
						<figcaption><?php the_field('section_one_image_caption'); ?></figcaption>
					</figure>

				</div>

				<div class="section-2">
					
					<div class="left">
						<p><?php the_field('section_two_body_one', false, false); ?></p>
						<p><?php the_field('section_two_body_two', false, false); ?></p>
					</div>

					<div class="right">

						<p><?php the_field('section_two_body_three', false, false); ?></p>

						<figure>
							<img src="<?php echo get_field('section_two_image')['url']; ?>">
							<figcaption><?php the_field('section_two_image_caption'); ?></figcaption>
						</figure>

						<p><?php the_field('section_two_body_four', false, false); ?></p>

					</div>

				</div>

				<div class="section-quote">
					
					<figure class="img-before">
						<img src="<?php echo get_field('section_three_image_one')['url'] ?>">
						<figcaption><?php the_field('section_three_image_one_caption'); ?></figcaption>
					</figure>

					<div class="quote-body">
						<p><svg class="svg-quote open">
							<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-open"></use>
						</svg><?php the_field('section_three_body', false, false); ?><svg class="svg-quote close">
							<use xlink:href="<?php echo get_template_directory_uri(); ?>/img/spritemap.svg#quote-close"></use>
						</svg></p>
						<p class="attribution">
							<?php the_field('quote_attribution', false, false); ?>
						</p>
					</div>

					<figure class="img-after">
						<img src="<?php echo get_field('section_three_image_two')['url']; ?>">
						<figcaption><?php the_field('section_three_image_two_caption'); ?></figcaption>
					</figure>

				</div>

				<div class="section-4">

					<p class="section-4-body"><?php the_field('section_four_body', false, false); ?></p>

					<figure>
						<img src="<?php echo get_field('section_four_image')['url']; ?>">
						<figcaption><?php the_field('section_four_image_caption'); ?></figcaption>
					</figure>

				</div>

			</div>

		</div><?php #main ?>

	</div><?php #primary ?>

<?php get_footer(); ?>